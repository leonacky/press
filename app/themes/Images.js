// @flow

// leave off @2x/@3x
const images = {
  logo: require('../images/ir.png'),
  logo2: require('../images/ir2.png'),
  clearLogo: require('../images/top_logo.png'),
  ignite: require('../images/ignite_logo.png'),
  tileBg: require('../images/tile_bg.png'),
  background: require('../images/BG.png'),
  loginBackground: require('../images/loginBackground.png'),
  pressMarketing: require('../images/pressMarketing.png'),
  check: require('../images/check.png')
}

export default images
